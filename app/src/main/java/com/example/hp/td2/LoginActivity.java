package com.example.hp.td2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {
    private EditText log, mdp;
    private TextView t1, t2;
    private Button btLog;
    private final static int EXIT_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        setTitle(getLocalClassName());
        t1 = (TextView) findViewById(R.id.textView);
        t2 = (TextView) findViewById(R.id.textView2);
        log = (EditText) findViewById(R.id.log);
        mdp = (EditText) findViewById(R.id.pwd);
        btLog = (Button) findViewById(R.id.butLog);


        btLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewListApplication app = (NewListApplication) getApplicationContext();
                app.setLogin(String.valueOf(log.getText()));
                act(v);
            }
        });

    }

    /*  @Override
      public void onClick(View v) {
          Intent intent;
          intent=new Intent(this,NewsActivity.class);
          intent.putExtra("log",log.getText());
          startActivity(intent);

      }*/
    public void act(View v) {

        Intent intent;
        intent = new Intent(this, NewsActivity.class);
        intent.putExtra("log", String.valueOf(log.getText()));
        startActivityForResult(intent,EXIT_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EXIT_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("EXIT", true)) {
                    finish();
                }
            }
        }


    }
}
