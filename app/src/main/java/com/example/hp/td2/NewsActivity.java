package com.example.hp.td2;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity {
    private TextView welcome;
    private Button btAb,btOut,btDet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news);
        setTitle(getLocalClassName());
        welcome = (TextView)findViewById(R.id.msgW);
        btAb=(Button)findViewById(R.id.btnAbout);
        btOut=(Button)findViewById(R.id.btnOut);
        btDet=(Button)findViewById(R.id.btnDet);

        Intent intent = getIntent();
        String lg = intent.getStringExtra("log");
        welcome.setText("Welcome "+lg);
        NewListApplication app = (NewListApplication) getApplicationContext();
        String login = app.getLogin();
        welcome.setText("Welcome "+login);

        btDet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent;
                intent=new Intent(NewsActivity.this,DetailsActivity.class);
                startActivity(intent);
            }
        });
        btOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1;
                intent1 = new Intent(NewsActivity.this,LoginActivity.class);
                startActivity(intent1);

            }
        });
        btAb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://android.busin.fr/";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        });

        onBackPressed();




    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        setResult(RESULT_OK, new Intent().putExtra("EXIT", true));
                        finish();
                    }

                }).create().show();
    }
}
