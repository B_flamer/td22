package com.example.hp.td2;

import android.app.Application;

public class NewListApplication extends Application {


    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    @Override
    public void onCreate()
    {
        super.onCreate();
        this.login = null;
    }

}
