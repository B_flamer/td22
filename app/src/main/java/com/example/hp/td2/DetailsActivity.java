package com.example.hp.td2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private TextView txt;
    private Button btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        setTitle(getLocalClassName());

        txt=(TextView) findViewById(R.id.txtV);
        btn=(Button)findViewById(R.id.bntBack);
        NewListApplication app = (NewListApplication) getApplicationContext();
        String login = app.getLogin();
        txt.setText("Login is : "+login);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(DetailsActivity.this,NewsActivity.class);
                startActivity(intent);

            }
        });

    }
}
